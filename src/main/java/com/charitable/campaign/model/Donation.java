package com.charitable.campaign.model;

import javax.persistence.*;

@Entity
@Table(name = "donation")
public class Donation {

    // ---- Class untuk tiap donasi yang udah berhasil ----

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "idDonation", updatable = false, nullable = false)
    private int idDonation;

    @Column(name = "type")
    private String type;

    @Column(name = "bankName")
    private String bankName;

    @Column(name = "accountNumber")
    private String accountNumber;

    @Column(name = "accountName")
    private String accountName;

    @Column(name = "donationAmount")
    private int donationAmount;

    public void setIdDonation(int idDonation) {
        this.idDonation = idDonation;
    }
    public int getIdDonation() {
        return idDonation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getDonationAmount() {
        return donationAmount;
    }

    public void setDonationAmount(int donationAmount) {
        this.donationAmount = donationAmount;
    }

    @Override
    public String toString() {
        return getIdDonation() + " - " +
                getType() + " - " +
                getBankName() + " - " +
                getAccountNumber() + " - " +
                getAccountName() + " - " +
                getDonationAmount();
    }

}
