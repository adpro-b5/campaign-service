package com.charitable.campaign.core;

public class EducationalCampaign extends Campaign{

    public EducationalCampaign() {
        this.like = 0;
    }

    @Override
    public String getOverview() {
        return "Everyone has the right to pursue their dreams, " +
                "but some of them can’t even go to school due to " +
                "lack of financial support.";
    }

    @Override
    public String getDescription() {
        return "Everyone has the right to pursue their dreams, " +
                "but some of them can’t even go to school due to lack of financial support. " +
                "They have to work to earn their own money while others at their age are busy " +
                "studying. In other case, there are school constructions which has not yet " +
                "been completed, especially in countryside that also need your help. " +
                "Your donation will be distributed to people who need money for educational " +
                "costs and also to educational foundations that still lack facilities or are " +
                "still under construction.";
    }
}
