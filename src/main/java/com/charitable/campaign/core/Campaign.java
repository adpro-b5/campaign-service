package com.charitable.campaign.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Campaign {

    protected int like;

    public List<String> showCampaign() {
        List<String> list = new ArrayList<>();
        list.add(getDescription());
        list.add(step());
        list.add(stepOne());
        list.add(stepTwo());
        list.add(stepThree());
        list.add(stepFour());
        list.add(stepFive());
        return list;
    }

    public abstract String getDescription();

    public abstract String getOverview();

    public int getLike() {
        return like;
    }

    public void addLike(boolean naik) {
        like += naik ? 1 : -1;
    }

    public String step() {
        return "Ready to donate? Follow these steps:";
    }

    public String stepOne() {
        return "- Click 'Donate Now' button below";
    }

    public String stepTwo() {
        return "- Donate your money by transferring to our bank account";
    }

    public String stepThree() {
        return "- Fill payment form with information of your bank account and the nominal";
    }

    public String stepFour() {
        return "- Click 'Confirm' and your donation will be sent";
    }

    public String stepFive() {
        return "- Share this campaign so that others can do good deeds too";
    }

}
