package com.charitable.campaign.core;

public class HealthCampaign extends Campaign{

    public HealthCampaign() {
        this.like = 0;
    }

    @Override
    public String getOverview() {
        return "The world is still suffered from viruses, bad nutrition, " +
                "diseases, and other health problems. Help them to get treatment " +
                "immediately.";
    }

    @Override
    public String getDescription() {
        return "Everyone deserves to be healthy and get treatment equally " +
                "but we could see that the world is still suffered from viruses, " +
                "bad nutrition, diseases, and many health problems. Most of the " +
                "untreated patients don’t have enough money to pay their bills. " +
                "Also hospitals and clinics in countryside are still lack of medical " +
                "equipment. Your donation will be sent to hospitals and clinics " +
                "in the city and countryside to help people in need for money to " +
                "get treatment and support them with medical equipments they need. ";
    }
}
