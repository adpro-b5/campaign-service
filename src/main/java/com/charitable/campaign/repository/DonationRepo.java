package com.charitable.campaign.repository;

import com.charitable.campaign.model.Donation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonationRepo extends JpaRepository<Donation, Integer> {

    Donation findByIdDonation(int idDonation);
}
