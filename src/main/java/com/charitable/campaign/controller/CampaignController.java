package com.charitable.campaign.controller;

import com.charitable.campaign.model.Donation;
import com.charitable.campaign.service.CampaignService;
import com.charitable.campaign.service.DonationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private DonationService donationService;

    @GetMapping("/home")
    public List<String> allCampaignHome() {
        return campaignService.campaignOverviews();
    }

    @GetMapping("/detail/edu")
    public Iterable<String> eduCampaignDetail() {
        return campaignService.showEduCampaign();
    }

    @GetMapping("/detail/health")
    public Iterable<String> healthCampaignDetail() {
        return campaignService.showHealthCampaign();
    }

    @GetMapping("/detail/social")
    public Iterable<String> socialCampaignDetail() {
        return campaignService.showSocialCampaign();
    }

    @GetMapping("/like/edu")
    public int eduLike() {
        return campaignService.eduLike();
    }

    @GetMapping("/like/social")
    public int socialLike() {
        return campaignService.socialLike();
    }

    @GetMapping("/like/health")
    public int healthLike() {
        return campaignService.healthLike();
    }

    @GetMapping("/create-donation/{type}")
    public Donation paymentCampaign(@PathVariable(value = "type") String type) {
        Donation donation = new Donation();
        donation.setType(type);

        return donation;
    }

    @PostMapping(path = "/confirm-donation")
    public void confirmPayment(@RequestBody Donation donation) {
        donationService.createDonation(donation);
    }

}
