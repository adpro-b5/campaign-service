package com.charitable.campaign.service;

import com.charitable.campaign.model.Donation;
import com.charitable.campaign.repository.DonationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class DonationServiceImpl implements DonationService {

    @Autowired
    private DonationRepo donationRepo;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Donation createDonation(Donation donation) {
        donationRepo.save(donation);
        notifyReport(donation.getType(), donation.getDonationAmount());
        return donation;
    }

    @Override
    public Iterable<Donation> getListDonation() {
        return donationRepo.findAll();
    }

    @Override
    public List<Donation> getListDonationByType(String type) {
        List<Donation> listDonation = new ArrayList<>();
        for (Donation donation : donationRepo.findAll()) {
            if (donation.getType().equals(type)) {
                listDonation.add(donation);
            }
        }
        return listDonation;
    }

    @Override
    public void notifyReport(String type, int donationAmount) {
        restTemplate.getForObject(
                "http://CHARITABLE-REPORT/notify/" + type +
                        "?donationAmount=" + donationAmount,
                void.class);
    }
}
