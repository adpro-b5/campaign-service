package com.charitable.campaign.service;

import com.charitable.campaign.core.Campaign;
import com.charitable.campaign.core.EducationalCampaign;
import com.charitable.campaign.core.HealthCampaign;
import com.charitable.campaign.core.SocialCampaign;

import java.util.List;

public interface CampaignService {

    Iterable<String> showEduCampaign();
    Iterable<String> showSocialCampaign();
    Iterable<String> showHealthCampaign();

    List<String> campaignOverviews();
    Integer eduLike();
    Integer socialLike();
    Integer healthLike();

    void like(String type, boolean add);
}
