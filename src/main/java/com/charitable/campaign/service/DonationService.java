package com.charitable.campaign.service;

import com.charitable.campaign.model.Donation;
import java.util.List;

public interface DonationService {
    Donation createDonation(Donation donation);
    Iterable<Donation> getListDonation();
    List<Donation> getListDonationByType(String type);
    void notifyReport(String type, int donationAmount);
}