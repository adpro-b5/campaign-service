package com.charitable.campaign.service;

import com.charitable.campaign.core.Campaign;
import com.charitable.campaign.core.EducationalCampaign;
import com.charitable.campaign.core.HealthCampaign;
import com.charitable.campaign.core.SocialCampaign;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {

    private final EducationalCampaign educationalCampaign;
    private final SocialCampaign socialCampaign;
    private final HealthCampaign healthCampaign;

    public CampaignServiceImpl() {
        educationalCampaign = new EducationalCampaign();
        socialCampaign = new SocialCampaign();
        healthCampaign = new HealthCampaign();
    }

    @Override
    public List<String> showEduCampaign() {
        return educationalCampaign.showCampaign();
    }

    @Override
    public List<String> showSocialCampaign() {
        return socialCampaign.showCampaign();
    }

    @Override
    public List<String> showHealthCampaign() {
        return healthCampaign.showCampaign();
    }

    @Override
    public List<String> campaignOverviews() {
        List<String> overviews = new ArrayList<>();
        overviews.add(educationalCampaign.getOverview());
        overviews.add(socialCampaign.getOverview());
        overviews.add(healthCampaign.getOverview());
        return overviews;
    }

    @Override
    public Integer eduLike() {
        return educationalCampaign.getLike();
    }

    @Override
    public Integer socialLike() {
        return socialCampaign.getLike();
    }

    @Override
    public Integer healthLike() {
        return healthCampaign.getLike();
    }

    @Override
    public void like(String type, boolean add) {
        if(type.equalsIgnoreCase("edu")) {
            educationalCampaign.addLike(add);
        } else if(type.equalsIgnoreCase("social")) {
            socialCampaign.addLike(add);
        } else {
            healthCampaign.addLike(add);
        }
    }


}
