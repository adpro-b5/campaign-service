package com.charitable.campaign.service;

import com.charitable.campaign.core.EducationalCampaign;
import com.charitable.campaign.core.HealthCampaign;
import com.charitable.campaign.core.SocialCampaign;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CampaignServiceImplTest {

    @InjectMocks
    private CampaignServiceImpl campaignService;

    private EducationalCampaign educationalCampaign;
    private SocialCampaign socialCampaign;
    private HealthCampaign healthCampaign;

    @BeforeEach
    public void setUp() {
        educationalCampaign = new EducationalCampaign();
        socialCampaign = new SocialCampaign();
        healthCampaign = new HealthCampaign();
    }

    @Test
    public void whenShowEduCampaignShouldEduShowCampaign() {
        Iterable<String> calledEduCampaign = campaignService.showEduCampaign();
        assertThat(calledEduCampaign).isEqualTo(educationalCampaign.showCampaign());
    }

    @Test
    public void whenShowSocialCampaignShouldSocialShowCampaign() {
        Iterable<String> calledSocialCampaign = campaignService.showSocialCampaign();
        assertThat(calledSocialCampaign).isEqualTo(socialCampaign.showCampaign());
    }

    @Test
    public void whenShowHealthCampaignShouldHealthShowCampaign() {
        Iterable<String> calledHealthCampaign = campaignService.showHealthCampaign();
        assertThat(calledHealthCampaign).isEqualTo(healthCampaign.showCampaign());
    }

    @Test
    public void testGetAllOverviews() {
        List<String> overviews = new ArrayList<>();
        overviews.add(educationalCampaign.getOverview());
        overviews.add(socialCampaign.getOverview());
        overviews.add(healthCampaign.getOverview());
        Assertions.assertEquals(overviews, campaignService.campaignOverviews());
    }

    @Test
    public void testGetLike() {
        Assertions.assertEquals(0,campaignService.eduLike());
        Assertions.assertEquals(0, campaignService.healthLike());
        Assertions.assertEquals(0,campaignService.socialLike());
    }

    @Test
    public void testLikes() {
        campaignService.like("edu", true);
        campaignService.like("edu", false);
        Assertions.assertEquals(0, campaignService.eduLike());
        campaignService.like("social", true);
        campaignService.like("social", false);
        Assertions.assertEquals(0, campaignService.socialLike());
        campaignService.like("health", true);
        campaignService.like("health", false);
        Assertions.assertEquals(0, campaignService.healthLike());
    }
}
