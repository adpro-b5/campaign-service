package com.charitable.campaign.service;

import com.charitable.campaign.model.Donation;
import com.charitable.campaign.repository.DonationRepo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class DonationServiceImplTest {

    @Mock
    private DonationRepo donationRepo;

    @InjectMocks
    private DonationServiceImpl donationService;

//    @Mock
//    private ReportService reportService;

    private static Donation donation = new Donation();

    @BeforeAll
    static void setUp() {
        donation.setType("Educational");
        donation.setBankName("bank");
        donation.setAccountName("abc");
        donation.setAccountNumber("123");
        donation.setDonationAmount(500);
    }

//    @Test
//    void testCreateDonation() {
//        lenient().when(donationRepo.save(any(Donation.class))).then(returnsFirstArg());
//        Donation newDonation = donationService.createDonation(donation);
//        assertEquals(donation, newDonation);
//    }

    @Test
    void testGetListDonation() {
        List<Donation> expectedList = Collections.singletonList(donation);
        lenient().when(donationRepo.findAll()).thenReturn(expectedList);
        Iterable<Donation> actualList = donationService.getListDonation();
        assertEquals(expectedList, actualList);
    }

    @Test
    void testGetListDonationByType() {
        List<Donation> expectedList = Collections.singletonList(donation);
        lenient().when(donationRepo.findAll()).thenReturn(expectedList);
        Iterable<Donation> actualList = donationService.getListDonationByType("Educational");
        assertEquals(expectedList, actualList);
    }
}

