package com.charitable.campaign.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HealthCampaignTest {
    private Class<?> healthCampaignClass;

    @BeforeEach
    public void setUp() throws Exception {
        healthCampaignClass = Class.forName("com.charitable.campaign.core.HealthCampaign");
    }

    @Test
    public void testEducationalCampaignIsConcreteClass() {
        assertFalse(Modifier.isAbstract(healthCampaignClass.getModifiers()));
    }

    @Test
    public void testEducationalCampaignIsACampaign() {
        Class<?> parentClass = healthCampaignClass.getSuperclass();
        assertEquals("com.charitable.campaign.core.Campaign", parentClass.getName());
    }

    @Test
    public void testOverrideGetOverview() throws Exception {
        Method getOverview = healthCampaignClass.getDeclaredMethod("getOverview");
        assertEquals("java.lang.String", getOverview.getGenericReturnType().getTypeName());
        assertEquals(0, getOverview.getParameterCount());
    }

    @Test
    public void testOverrideGetDescription() throws Exception {
        Method getDescription = healthCampaignClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testReturnOverview() {
        HealthCampaign healthCampaign = new HealthCampaign();
        String hasil = "The world is still suffered from viruses, bad nutrition, " +
                "diseases, and other health problems. Help them to get treatment " +
                "immediately.";
        assertEquals(hasil, healthCampaign.getOverview());
    }

    @Test
    public void testReturnGetLike() {
        HealthCampaign healthCampaign = new HealthCampaign();
        assertEquals(0, healthCampaign.getLike());
        healthCampaign.addLike(true);
        assertEquals(1, healthCampaign.getLike());
        healthCampaign.addLike(false);
        assertEquals(0, healthCampaign.getLike());
    }
}
