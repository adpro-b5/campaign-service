package com.charitable.campaign.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class SocialCampaignTest {
    private Class<?> socialCampaignClass;

    @BeforeEach
    public void setUp() throws Exception {
        socialCampaignClass = Class.forName("com.charitable.campaign.core.SocialCampaign");
    }

    @Test
    public void testEducationalCampaignIsConcreteClass() {
        assertFalse(Modifier.isAbstract(socialCampaignClass.getModifiers()));
    }

    @Test
    public void testEducationalCampaignIsACampaign() {
        Class<?> parentClass = socialCampaignClass.getSuperclass();
        assertEquals("com.charitable.campaign.core.Campaign", parentClass.getName());
    }

    @Test
    public void testOverrideGetOverview() throws Exception {
        Method getOverview = socialCampaignClass.getDeclaredMethod("getOverview");
        assertEquals("java.lang.String", getOverview.getGenericReturnType().getTypeName());
        assertEquals(0, getOverview.getParameterCount());
    }

    @Test
    public void testOverrideGetDescription() throws Exception {
        Method getDescription = socialCampaignClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testReturnOverview() {
        SocialCampaign socialCampaign = new SocialCampaign();
        String hasil = "People's lives, destiny, luck, are not the same. " +
                "Poverty and disasters are keep happening. Humanity is calling you here now.";
        assertEquals(hasil, socialCampaign.getOverview());
    }

    @Test
    public void testReturnGetLike() {
        SocialCampaign socialCampaign = new SocialCampaign();
        assertEquals(0, socialCampaign.getLike());
        socialCampaign.addLike(true);
        assertEquals(1, socialCampaign.getLike());
        socialCampaign.addLike(false);
        assertEquals(0, socialCampaign.getLike());
    }
}
