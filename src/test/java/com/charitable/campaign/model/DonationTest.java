package com.charitable.campaign.model;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DonationTest {

    private Donation donation;

    @BeforeEach
    public void setUp()throws Exception {
        this.donation = new Donation();
    }

    @Test
    void testSetBankNameMethod() throws Exception {
        donation.setBankName("namabank");
        String hasil = donation.getBankName();
        Assertions.assertEquals("namabank", hasil);
    }

    @Test
    void testSetAccountNumberMethod() throws Exception {
        donation.setAccountNumber("1234567890");
        String hasil = donation.getAccountNumber();
        Assertions.assertEquals("1234567890", hasil);
    }

    @Test
    void testSetAccountNameMethod() throws Exception {
        donation.setAccountName("namaakun");
        String hasil = donation.getAccountName();
        Assertions.assertEquals("namaakun", hasil);
    }

    @Test
    void testSetDonationAmountMethod() throws Exception {
        donation.setDonationAmount(50000);
        int hasil = donation.getDonationAmount();
        Assertions.assertEquals(50000, hasil);
    }

    @Test
    void testGetIdDonationMethod() throws Exception {
        int hasil = donation.getIdDonation();
        Assertions.assertEquals(0,hasil);
    }

    @Test
    void testToStringReturn() throws Exception {
        String hasil = donation.toString();
        String expect = "0 - null - null - null - null - 0";
        Assertions.assertEquals(expect, hasil);
    }

    @Test
    void testSetterGetter() throws Exception {
        donation.setIdDonation(1);
        donation.setDonationAmount(20000);
        donation.setAccountName("nama");
        donation.setAccountNumber("12345");
        donation.setBankName("bank");
        donation.setType("Educational");
        Assertions.assertEquals(1, donation.getIdDonation());
        Assertions.assertEquals(20000, donation.getDonationAmount());
        Assertions.assertEquals("nama", donation.getAccountName());
        Assertions.assertEquals("12345", donation.getAccountNumber());
        Assertions.assertEquals("bank", donation.getBankName());
        Assertions.assertEquals("Educational", donation.getType());
        String toString = "1 - Educational - bank - 12345 - nama - 20000";
        Assertions.assertEquals(toString, donation.toString());
    }
}
