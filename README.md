# Campaign Service
[![pipeline status](https://gitlab.com/adpro-b5/campaign-service/badges/master/pipeline.svg)](https://gitlab.com/adpro-b5/campaign-service/-/commits/master)
[![coverage report](https://gitlab.com/adpro-b5/campaign-service/badges/master/coverage.svg)](https://gitlab.com/adpro-b5/campaign-service/-/commits/master)

Deployed on https://charitable-campaign.herokuapp.com
